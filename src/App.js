import './App.css';
import {Route, Switch, Redirect} from "react-router-dom";

//components
import Store from "./components/Store";
import ProductsDetails from './components/ProductsDetails';
import Navbar from './components/shared/Navbar';
import ShopCart from './components/ShopCart';


//CONTEXT
import ProductContextProvider from './context/ProductContextProvider';
import CartContextProvider from './context/CartContextProvider';

function App() {
  return (
    <ProductContextProvider>
      <CartContextProvider>
        <Navbar />
        <Switch>
          <Route path="/products/:id" component={ProductsDetails}/>
          <Route path="/products" component={Store}/>
          <Route path="/shopcart" component={ShopCart}/>
          <Redirect to="/products" />
        </Switch>
      </CartContextProvider>
    </ProductContextProvider>
  );
}

export default App;
