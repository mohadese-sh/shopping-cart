import React, { useContext } from 'react';

import styles from "./Navbar.module.css";

//context
import { CartContext } from '../../context/CartContextProvider';

//Icons
import shopIcon from "../../assets/icons/shop.svg";
import { Link } from 'react-router-dom';

const Navbar = () => {


    const {state} = useContext(CartContext)

    return (
        <div className={styles.maincontainer}>
            <div className={styles.container}>
                <Link to="/products" className={styles.productLink}>Products</Link>
                <div className={styles.iconContainer}>
                    <Link to="shopcart"><img src={shopIcon} alt="shop" /></Link>
                    <span>{state.itemsCounter}</span>
                </div>
            </div>
        </div>
    );
};

export default Navbar;